//
//  API.swift
//  IdusTestProject
//
//  Created by 김승율 on 2021/03/24.
//

import Foundation
import Alamofire
import SwiftyJSON

let BaseURL = "http://itunes.apple.com/lookup?id=872469884"

//인디게이터 초기화
var indicator:Indicator!{
    let indicator = Indicator(frame: appDelegate.window!.frame)
    return indicator
}

//Get 통신
func getApiNetwork(url:String, success: @escaping(JSON) -> (), failure: @escaping(AFError) -> ()) {
    indicator!.showIndicator()
    print("URL 주소: \(url)")
    DispatchQueue.global(qos: .userInitiated).async {
        AF.request(URL.init(string: url)!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            
            switch (response.result) {
            case .success(let json):
                DispatchQueue.main.async {
                    indicator!.hideIndicator()
                    let jsonString = String(describing: json)
                    print("통신성공:"+jsonString)
                    let jsonData = JSON(json)
                    success(jsonData)
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    indicator!.hideIndicator()
                    print("통신실패:\(error)")
                    failure(error)
                }
            }
        }
    }
}

