//
//  Utill.swift
//  IdusTestProject
//
//  Created by 김승율 on 2021/03/24.
//

import Foundation
import UIKit

//디바이스 크기
let screenWidth = UIScreen.main.bounds.width
let screenHeight = UIScreen.main.bounds.height

//AppDelegate 설정
var appDelegate:AppDelegate {
    return UIApplication.shared.delegate as! AppDelegate
}

//알럿창
func showAlert(message:String) {
    let alert = UIAlertController(title: "알림", message: message, preferredStyle: .alert)
    
    let ok = UIAlertAction(title: "확인", style: .default, handler: { action in})
    alert.addAction(ok)
    
    DispatchQueue.main.async{
        var rootViewController = appDelegate.window?.rootViewController
        if let navigationController = rootViewController as? UINavigationController {
            rootViewController = navigationController.viewControllers.first
        }
        if let tabBarController = rootViewController as? UITabBarController {
            rootViewController = tabBarController.selectedViewController
        }
        rootViewController?.present(alert, animated: true, completion: nil)
    }
}
