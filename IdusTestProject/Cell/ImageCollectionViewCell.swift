//
//  ImageCollectionViewCell.swift
//  IdusTestProject
//
//  Created by 김승율 on 2021/03/24.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgView: UIImageView! //스크린샷 이미지
}
