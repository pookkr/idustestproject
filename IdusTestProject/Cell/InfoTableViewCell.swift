//
//  InfoTableViewCell.swift
//  IdusTestProject
//
//  Created by 김승율 on 2021/03/25.
//

import UIKit

class InfoTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLbl: UILabel! //제목
    @IBOutlet weak var contentsLbl: UILabel! //내용
    @IBOutlet weak var arrowImgView: UIImageView! //화살표이미지
    @IBOutlet weak var releaseNotesLbl: UILabel! //업데이트 내용
    @IBOutlet weak var underLineView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
