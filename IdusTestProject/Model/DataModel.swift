//
//  DataModel.swift
//  IdusTestProject
//
//  Created by 김승율 on 2021/03/24.
//

import UIKit
import SwiftyJSON

class DataModel: NSObject {
    var screenshotUrls:[JSON]? //스크린샷 이미지 url 리스트
    var fileSizeBytes:Float? //파일용량
    var version:String? //버전
    var releaseNotes:String? //업데이트 내용
    var descriptionString:String? //설명
    var genres:[JSON]? //카테고리 리스트
}
