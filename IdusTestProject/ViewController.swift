//
//  ViewController.swift
//  IdusTestProject
//
//  Created by 김승율 on 2021/03/24.
//

import UIKit

class ViewController: UIViewController {
    
    private var infoData:DataModel = DataModel()
    @IBOutlet weak var imageCollectionView: UICollectionView! //이미지리스트 뷰
    @IBOutlet weak var infoTableView: UITableView! //크기,버전정보 뷰
    @IBOutlet weak var infoTableViewHeightCon: NSLayoutConstraint! //크기,버전정보 뷰 높이
    @IBOutlet weak var descriptionLbl: UILabel! //설명글
    @IBOutlet weak var categoryCollectionView: UICollectionView! //카테고리 뷰
    @IBOutlet weak var categoryCollectionViewHeightCon: NSLayoutConstraint! //카테고리 뷰 높이
    
    var selectedCellIndexPath: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "아이디어스(idus)"
        getNetworkData()
    }
    
    //Data API 통신
    func getNetworkData(){
        getApiNetwork(url: BaseURL, success: {result in
            //네트워크 통신 성공시 처리
            let data = result["results"].arrayValue[0]
            self.infoData.screenshotUrls = data["screenshotUrls"].arrayValue
            self.infoData.fileSizeBytes = data["fileSizeBytes"].floatValue
            self.infoData.version = data["version"].stringValue
            self.infoData.releaseNotes = data["releaseNotes"].stringValue
            self.infoData.descriptionString = data["description"].stringValue
            self.descriptionLbl.text = data["description"].stringValue
            self.infoData.genres = data["genres"].arrayValue
            self.imageCollectionView.reloadData()
            self.infoTableView.reloadData()
            self.categoryCollectionView.reloadData()
        }, failure: {result in
            //네트워크 통신 오류시 처리
            showAlert(message: "네트워크 오류")
        })
    }
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == imageCollectionView){
            //스크린샷 이미지 콜렉션 뷰일경우
            return infoData.screenshotUrls?.count ?? 0
        }else{
            //카테고리 콜렉션 뷰일경우
            return infoData.genres?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(collectionView == imageCollectionView){
            //스크린샷 이미지 콜렉션 뷰일경우
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as! ImageCollectionViewCell
            cell.imgView.setImage(with: infoData.screenshotUrls?[indexPath.row].stringValue ?? "")
            return cell
        }else{
            //카테고리 콜렉션 뷰일경우
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionViewCell", for: indexPath) as! CategoryCollectionViewCell
            cell.titleLbl.text = "#" + (infoData.genres?[indexPath.row].stringValue ?? "")
            cell.contentView.layer.cornerRadius = 5
            cell.contentView.layer.borderWidth = 0.5
            cell.contentView.layer.borderColor = UIColor.lightGray.cgColor
            self.categoryCollectionViewHeightCon.constant = self.categoryCollectionView.contentSize.height
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(collectionView == imageCollectionView){
            //스크린샷 이미지 콜렉션 뷰일경우
            return CGSize(width: screenWidth, height: 400)
        }else{
            //카테고리 콜렉션 뷰일경우
            return collectionView.contentSize
        }
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InfoTableViewCell", for: indexPath) as! InfoTableViewCell
        if(indexPath.row == 0){
            cell.titleLbl.text = "크기"
            let mb = (infoData.fileSizeBytes ?? 0)/1024/1024
            cell.contentsLbl.text = String(format: "%.1f", mb) + " MB"
            cell.arrowImgView.isHidden = true
            cell.underLineView.isHidden = false
        }else{
            cell.titleLbl.text = "새로운 기능"
            cell.contentsLbl.text = infoData.version
            cell.contentsLbl.trailingAnchor.constraint(equalTo: cell.contentView.trailingAnchor, constant: -35).isActive = true
            cell.arrowImgView.isHidden = false
            cell.underLineView.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedCellIndexPath == indexPath {
            return UITableView.automaticDimension
        }
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! InfoTableViewCell
        
        guard indexPath.row == 1 else {
            return
        }
        
        if selectedCellIndexPath != nil && selectedCellIndexPath == indexPath  {
            cell.arrowImgView.image = UIImage(systemName: "arrow.down")
            cell.releaseNotesLbl.text = ""
            cell.underLineView.isHidden = true
            selectedCellIndexPath = nil
        } else {
            cell.arrowImgView.image = UIImage(systemName: "arrow.up")
            cell.releaseNotesLbl.text = infoData.releaseNotes
            cell.underLineView.isHidden = false
            
            selectedCellIndexPath = indexPath
        }
        
        tableView.beginUpdates()
        tableView.endUpdates()
        infoTableViewHeightCon.constant = 80 + cell.frame.size.height
    }
}
