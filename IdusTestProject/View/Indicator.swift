//
//  Indicator.swift
//  TodosDialer
//
//  Created by 김승율 on 2021/03/24.
//

import UIKit

class Indicator: UIView {
    @IBOutlet weak var indicator: UIActivityIndicatorView! //인디게이터
    @IBOutlet weak var view: UIView! //흑백 배경화면
    
    private let xibName = "Indicator"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    private func commonInit(){
        let view = Bundle.main.loadNibNamed(xibName, owner: self, options: nil)?.first as! UIView
        view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        self.addSubview(view)
    }
    
    //인디게이터 실행
    func showIndicator(){
        appDelegate.window?.addSubview(self.view)
        self.view.tag = 100
        self.indicator.startAnimating()
    }
    
    //인디게이터 중지
    func hideIndicator(){
        self.indicator.stopAnimating()
        appDelegate.window?.viewWithTag(100)?.removeFromSuperview()
    }
    
}
